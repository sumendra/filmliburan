package com.example.filmliburan.Preview.Detail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.filmliburan.Data.Model.TvShow;
import com.example.filmliburan.R;
import com.squareup.picasso.Picasso;

import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.CONTENT_URI;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.DESKRIPSI;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.GAMBARPOPULE;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.GENRE;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.JUDUL;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.ORIGINALLANGUAGE;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.POPULER;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.RELEASEDATE;

public class DetailTvShowActivity extends AppCompatActivity {

    TextView textJudul;
    TextView textPopuler;
    ImageView gambarPopuler;
    TvShow tvShow;
    TextView textDeskripsi;
    TextView textOriginalLanguage;
    TextView textGenre;
    TextView textReleaseDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_tv_show);
        textJudul= findViewById(R.id.text_judul);
        textPopuler= findViewById(R.id.text_populer);
        gambarPopuler= findViewById(R.id.gambar_populer);
        tvShow= getIntent().getParcelableExtra("intent");
        textJudul.setText(tvShow.getJudul());
        textPopuler.setText(String.valueOf(tvShow.getPopuler()));
        Picasso.get().load(tvShow.getGambar()).into(gambarPopuler);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        textDeskripsi= findViewById(R.id.text_deskripsi);
        textOriginalLanguage= findViewById(R.id.text_language);
        textGenre= findViewById(R.id.text_genre);
        textReleaseDate= findViewById(R.id.text_date);
        textGenre.setText(String.valueOf(tvShow.getGenre()));
        textDeskripsi.setText(tvShow.getDeskripsi());
        textOriginalLanguage.setText(tvShow.getOriginalLanguage());
        textReleaseDate.setText(tvShow.getReleaseDate());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.favorite_detail:
                tampungData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void tampungData(){
        ContentValues values= new ContentValues();
        values.put(JUDUL,tvShow.getJudul());
        values.put(DESKRIPSI, tvShow.getDeskripsi());
        values.put(GAMBARPOPULE, tvShow.getGambar());
        values.put(POPULER, tvShow.getPopuler());
        values.put(ORIGINALLANGUAGE, tvShow.getOriginalLanguage());
        values.put(GENRE, tvShow.getGenre());
        values.put(RELEASEDATE, tvShow.getReleaseDate());
        getContentResolver().insert(CONTENT_URI, values);
    }
}
