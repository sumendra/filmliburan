package com.example.filmliburan.Preview.Detail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.filmliburan.Data.Model.Movie;
import com.example.filmliburan.Data.Model.TvShow;
import com.example.filmliburan.R;
import com.squareup.picasso.Picasso;

import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.CONTENT_URI;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.DESKRIPSI;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.GAMBARPOPULE;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.GENRE;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.JUDUL;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.ORIGINALLANGUAGE;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.POPULER;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.RELEASEDATE;

public class DetailActivity extends AppCompatActivity {

    TextView textJudul;
    TextView textDeskripsi;
    ImageView gambarFilm;
    Movie movie;
    TextView populer;
    TextView originalLeangue;
    TextView genre;
    TextView releaseDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        textJudul= findViewById(R.id.text_judul);
        textDeskripsi= findViewById(R.id.text_deskripsi);
        gambarFilm= findViewById(R.id.gambar_film);
        movie = getIntent().getParcelableExtra("film");
        textJudul.setText(movie.getJudul());
        textDeskripsi.setText(movie.getDeskripsi());
        Picasso.get().load(movie.getGambar()).into(gambarFilm);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        populer= findViewById(R.id.text_populer);
        originalLeangue= findViewById(R.id.text_originalLanguage);
        genre= findViewById(R.id.text_genre);
        releaseDate= findViewById(R.id.text_releasedate);
        populer.setText(String.valueOf(movie.getPopuler()));
        originalLeangue.setText(movie.getOriginalLanguege());
        genre.setText(String.valueOf(movie.getGenre()));
        releaseDate.setText(movie.getReleaseDate());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.favorite_detail:
                tampungData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void tampungData(){
        ContentValues values= new ContentValues();
        values.put(JUDUL,movie.getJudul());
        values.put(DESKRIPSI, movie.getDeskripsi());
        values.put(GAMBARPOPULE, movie.getGambar());
        values.put(POPULER, movie.getPopuler());
        values.put(ORIGINALLANGUAGE, movie.getOriginalLanguege());
        values.put(GENRE, movie.getGenre());
        values.put(RELEASEDATE, movie.getReleaseDate());
        getContentResolver().insert(CONTENT_URI, values);
    }
}
