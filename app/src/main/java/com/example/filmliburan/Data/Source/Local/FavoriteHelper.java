package com.example.filmliburan.Data.Source.Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import static android.provider.BaseColumns._ID;

import static com.example.filmliburan.Data.Source.Local.DatabaseContract.TABLE_NAME;

public class FavoriteHelper {
    private static final String DATABASE_TABLE= TABLE_NAME;
    private static DatabaseHelper databaseHelper;
    private static FavoriteHelper INSTANCE;
    private static SQLiteDatabase database;
    private FavoriteHelper (Context context){
        databaseHelper= new DatabaseHelper(context);
    }

    public static FavoriteHelper getInstace(Context context){
        if(INSTANCE == null){
            synchronized (SQLiteDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE= new FavoriteHelper(context);
                }
            }
        }
        return INSTANCE;
    }
    public void open() throws SQLException{
        database= databaseHelper.getWritableDatabase();
    }
    public void close(){
        databaseHelper.close();
        if(database.isOpen()){
            database.close();
        }
    }

    public Cursor queryByIdProvider(String id){
        return database.query(DATABASE_TABLE, null
        , _ID + " =? "
        , new String[]{}
        , null
        , null
        , null
        , null);
    }
    public Cursor queryProvider(){
        return database.query(DATABASE_TABLE
        ,null
        ,null
        ,null
        ,null
        , null
        , _ID + " ASC ");
    }
    public long insertProvider(ContentValues contentValues){
        return database.insert(DATABASE_TABLE, null, contentValues);
    }
    public int updateProvider(String id, ContentValues values){
        return database.update(DATABASE_TABLE, values, _ID + " =? ", new String []{id});
    }
    public int deleteProvider(String id){
        return database.delete(DATABASE_TABLE, _ID + " =? ", new String []{id});
    }
}
