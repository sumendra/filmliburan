package com.example.filmliburan.Data.Source.Local;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY= "com.example.filmliburan";
    public static final String SCHEME= "content";
    public static String TABLE_NAME= "favorite";
    public static final class FavoriteColumn implements BaseColumns{
        public static String JUDUL= "judul";
        public static String DESKRIPSI= "deskripsi";
        public static String GAMBARPOPULE= "gambar_populer";
        public static String POPULER = "populer";
        public static String ORIGINALLANGUAGE= "original_language";
        public static String GENRE= "genre";
        public static String RELEASEDATE= "release_date";
        public static Uri CONTENT_URI= new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build();
    }
    public static String getColumnString(Cursor cursor, String columnName){
        return cursor.getString(cursor.getColumnIndex(columnName));
    }
    public static int getColumnInt(Cursor cursor, String columnName){
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
    public static long getColumnLong(Cursor cursor, String columnName){
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }
}
