package com.example.filmliburan.Data.Source.Local;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.filmliburan.Preview.Main.MainActivity;

import java.util.logging.Handler;

import static com.example.filmliburan.Data.Source.Local.DatabaseContract.AUTHORITY;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.FavoriteColumn.CONTENT_URI;
import static com.example.filmliburan.Data.Source.Local.DatabaseContract.TABLE_NAME;

public class FavoriteProvider extends ContentProvider {

    private static final int FAVORITE=1;
    private static final int FAVORITE_ID=2;
    private static final UriMatcher matcher= new UriMatcher(UriMatcher.NO_MATCH);
    private FavoriteHelper favoriteHelper;

    static {
        matcher.addURI(AUTHORITY, TABLE_NAME, FAVORITE);
        matcher.addURI(AUTHORITY, TABLE_NAME + "/#", FAVORITE_ID);
    }
    @Override
    public boolean onCreate() {
        favoriteHelper= FavoriteHelper.getInstace(getContext())
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        favoriteHelper.open();
        Cursor cursor;
        switch (matcher.match(uri)){
            case FAVORITE:
                cursor= favoriteHelper.queryProvider();
                break;
            case FAVORITE_ID:
                cursor= favoriteHelper.queryByIdProvider(uri.getLastPathSegment());
                break;
                default:
                    cursor=null;
                    break;
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        favoriteHelper.open();
        long added;
        switch (matcher.match(uri)){
            case FAVORITE:
                added= favoriteHelper.insertProvider(values);
                break;
                default:
                    added=0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(),getContext()));
        return Uri.parse(CONTENT_URI + "/"+ added);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        favoriteHelper.open();
        int deleted;
        switch (matcher.match(uri)){
            case FAVORITE_ID:
                deleted= favoriteHelper.deleteProvider(uri.getLastPathSegment());
                break;;
                default:
                    deleted=0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(),getContext()));
        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        favoriteHelper.open();
        int update;
        switch (matcher.match(uri)){
            case FAVORITE_ID:
                update= favoriteHelper.updateProvider(uri.getLastPathSegment(), values);
                break;
                default:
                    update=0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity().DataObserver(new Handler(), getContext()));
        return update;
    }
}
